

enum WebEvent { PageLoad, PageUnload, Test }
fn inspect(mut event: WebEvent) {
    match (event) {
        WebEvent::PageLoad => {
            print!("page loaded\n");
        }
        WebEvent::PageUnload => {
            print!("page unloaded\n");
        }
        _ => {
            print!("not recognised\n");
        }
    }
}

fn main() {
    let mut load: WebEvent = WebEvent::PageLoad;
    let mut unload: WebEvent = WebEvent::PageUnload;
    let mut test: WebEvent;
    test = WebEvent::Test;
    inspect(load);
    inspect(unload);
    inspect(test);
}

