#! /usr/bin/python
"""
C to Rust Transcompiler

Lluis Alonso Jane
Universitat de Barcelona
"""

from __future__ import print_function
import sys, os
from pre_processor import pre_process, prepare_dependencies
try:
    from pycparser import parse_file
    from rust_visitor import RustVisitor
except ImportError:
    print("ERROR: pycparser needs to be installed for the program to work. Please install it \
    \n(read the README for further instructions).")
    exit()

#pylint: disable=C0103
def main():
    """Main function"""

    keep_includes = True
    include_defines = False
    silent_mode = True

    #Launch options and errors
    if "-h" in sys.argv:
        print("\tUsage is \">python c-to-rs.py [filename]\".")
        print("\tOptions:")
        print("\t\t-h\tSee this message.")
        print("\t\t-ni\tRemove all includes from the file. Only functions from the original file \
               \n\t\t\twill be declared. \
               \n\t\t\tKeep in mind that this may break your program. This option is off by \
               \n\t\t\tdefault.")
        print("\t\t-d\tInclude the #defines as constants in the file. This option is off by \
               \n\t\t\tdefault.")
        print("\t\t-v\tShow the AST tree and the post-transpiled code. This option is off by \
               \n\t\t\tdefault.")
        sys.argv.remove("-h")
        exit()
    if "-ni" in sys.argv:
        keep_includes = False
        sys.argv.remove("-ni")
    if "-v" in sys.argv:
        silent_mode = False
        sys.argv.remove("-v")
    if "-d" in sys.argv:
        include_defines = True
        sys.argv.remove("-d")
    if len(sys.argv) < 2:
        print("\tUsage is \">python c-to-rs.py [filename]\".")
        print("\tSee \">python c-to-rs.py -h\" for more options")
        exit()

    #we need the absolute dir path to be able to execute remotely
    if getattr(sys, 'frozen', False):
        application_path = str(os.path.dirname(sys.executable))
    else:
        application_path = str(os.path.dirname(__file__))

    if application_path != "":
        application_path += "/"

    write_buffer = ""
    define_buffer = ""
    with open(sys.argv[1]) as file:
    #Preprocess the file to remove unwanted includes and include defines if need be
        write_buffer, define_buffer = pre_process(file, application_path, include_defines, \
                                                  keep_includes)



    #Prepare the dependencies file
    prepare_dependencies(application_path, application_path + "out/Cargo.toml")

    #File without the std includes and other preprocessing done
    with open("temp.c", mode="w") as temp_file:
        temp_file.write(write_buffer)

    #pycparser parse_file
    AST = parse_file("temp.c", use_cpp=True, cpp_path='gcc', cpp_args='-E')

    #Generate the base result
    rust_result = RustVisitor(application_path).visit(AST)
    #Add the necessary defines
    rust_result = define_buffer + rust_result
    if not silent_mode:
        AST.show()
        print(rust_result)

    #Write the final file
    with open(application_path + "out/src/main.rs", mode="w") as rust_file:
        rust_file.write(rust_result)

    #Remove temp file if exists
    try:
        os.remove("temp.c")
    except OSError:
        pass
if __name__ == "__main__":
    main()

"""TODOS:
  -Fer typechecks en operacions per a evitar errors de tipus (mirar en el binaryOp, mantenir una llibreria de funcions amb el tipus que retorna)
  -Implementar Unions
  -Testing de casos amb referencies explicites (&), ja que actualment s'ignoren (en un principi tot el que es passa per referencia i tambe ha de ser
   aixi en Rust ja es passa, la resta no cal que sigui passat per referencia).

  (Lower prio) 
  -Testing amb CSmith (code generator) per a trobar errors.
  -Passar un analisi previ per veure quines variables son mutables i quines son immutables (cargo clippy | rustfix ).
"""
