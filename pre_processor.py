"""
Preprocessor functions for the Rust transpiler

Lluis Alonso Jane
Universitat de Barcelona
"""

import re
import json

def is_float(value):
    """Given a value, checks if it can be
       converted to float"""
    try:
        float(value)
        return True
    except ValueError:
        return False

def is_int(value):
    """Given a value, checks if it can be
       converted to int"""
    try:
        int(value)
        return True
    except ValueError:
        return False

def get_type(element):
    """Given an element, parses it and returns
       its type"""
    if is_float(element):
        if is_int(element):
            #if it can be a float and an int, we
            #pick the one that doesn't lose precision
            #(int has better precision than float when
            # it comes to integer numbers)
            if abs(float(element)) > abs(int(element)):
                return "f64"
            return "i32"
        return "f64"
    return None

def parse_define(line):
    """Parses a line containing #define
       and returns corresponding Rust code"""
    line = line.replace("#define ", "")
    define = ""
    if "(" not in line:
        elements = line.split()
        tipe = get_type(elements[1])
        name = elements[0]
        define += "const " + name + " : " + tipe
        define += " = " + elements[1] + ";\n"
    else:
        pass
    return define

def pre_process(lines, application_path, include_defines=False, include_includes=True):
    """Given the lines of a FILE, returns a version of it ready to be
       parsed by the pycparser, together with other code that may be
       added to the top of the final Rust file"""

    write_buffer = ""
    define_buffer = ""
    search_for_return = False
    return_indent = 0
    dependencies = get_dependencies(application_path)
    if dependencies is not None:
        define_buffer += dependencies + "\n"
    for line in lines:
        #This block is to remove the int from the main function
        #It also removes the return statement if there's any
        if "main(" in line:
            line = re.sub(r".*main", "void main", line)
            search_for_return = True
        if search_for_return and "{" in line:
            return_indent += 1
        if search_for_return and "}" in line:
            return_indent -= 1
        if search_for_return and return_indent < 1:
            search_for_return = False
        if search_for_return and "return" in line:
            line = ""
            search_for_return = False

        #We only add the lines that are not includes from the stdlib
        #unless specified otherwise via a command option
        if "#include" not in line and line:
            write_buffer += line
        if "#include" in line and "<" not in line and include_includes:
            write_buffer += line

        #If there's a define, it's processed and converted to later
        #on add directly at the begining of the Rust fine. This will
        #only happen if it is specified explicitly via a command option
        if "#define" in line and include_defines:
            define_buffer += parse_define(line)

    return write_buffer, define_buffer

#This error is triggered here because pylint can't detect the contents
#of an array. It is disabled for linting purposes
def prepare_dependencies(application_path, filename=None):
    """Parses the dependencies file and prepares the Cargo.toml
       for the building process"""

    #the header for the dependencies manager
    #this part is invariant, and should be always present
    header = "[package] \nname = \"out\" \nversion = \"0.1.0\" \
             \nauthors = [\"c-to-rs\"] \n\n[dependencies]\n"
    dependencies = ""

    with open(application_path + "dependencies.json") as json_file:
        json_data = json.load(json_file)
    crates = json_data["crates"]
    dependencies += '\n'.join(next(iter(crate)) + " = \"" + \
                    crate[next(iter(crate))]["version"] + "\""  for crate in crates\
                    if crate[next(iter(crate))]["use"])

    #the dependencies are saved to the file
    if filename:
        with open(filename, "w") as dep_file:
            dep_file.write(header+dependencies)

def get_dependencies(application_path):
    """Returns the dependencies to be declared in the code"""
    dependencies = ""

    with open(application_path + "dependencies.json") as json_file:
        json_data = json.load(json_file)
    crates = json_data["crates"]

    dependencies += '\n'.join(dependencies_modifiers(crate[next(iter(crate))]) + \
                    "extern crate " + next(iter(crate)) + ";" for crate in crates \
                    if crate[next(iter(crate))]["use"]) + "\n"
    return dependencies

def dependencies_modifiers(crate):
    """Returns the compiler flags for the dependencies importing"""
    modifiers = ""
    modifiers = " ".join(mod for mod in crate.keys() if mod not in ["version", "use"] \
                   and crate[mod])
    #beauty modifier so the crate declaration is not glued to the modifier
    if modifiers:
        modifiers += " "
    return modifiers

#For debugging purposes
#TODO: remove when it's done
if __name__ == "__main__":
    print(get_dependencies("."))
