\select@language {spanish}
\select@language {english}
\select@language {english}
\select@language {catalan}
\select@language {spanish}
\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {subsection}{\numberline {1.1}Motivation}{3}
\contentsline {subsection}{\numberline {1.2}Organization}{4}
\contentsline {section}{\numberline {2}Background}{5}
\contentsline {subsection}{\numberline {2.1}\textit {Rust}}{5}
\contentsline {subsection}{\numberline {2.2}Compilers}{6}
\contentsline {subsection}{\numberline {2.3}Transcompilers}{7}
\contentsline {subsubsection}{\numberline {2.3.1}Previous work}{7}
\contentsline {subsubsection}{\numberline {2.3.2}Tools used}{8}
\contentsline {section}{\numberline {3}Architecture and design}{9}
\contentsline {subsection}{\numberline {3.1}Architecture}{9}
\contentsline {subsubsection}{\numberline {3.1.1}Work done and usage}{10}
\contentsline {subsection}{\numberline {3.2}Analysis of a transcompilation example}{11}
\contentsline {subsection}{\numberline {3.3}Details of the code generation from the previous example}{14}
\contentsline {subsection}{\numberline {3.4}Files and customization}{22}
\contentsline {section}{\numberline {4}Tests and examples}{24}
\contentsline {subsection}{\numberline {4.1}What is working}{24}
\contentsline {subsection}{\numberline {4.2}Limitations}{26}
\contentsline {subsection}{\numberline {4.3}Examples}{28}
\contentsline {section}{\numberline {5}Conclusions}{34}
\contentsline {subsection}{\numberline {5.1}Future work}{34}
