\contentsline {section}{\numberline {1}Introducci\IeC {\'o}}{3}
\contentsline {subsection}{\numberline {1.1}Context}{3}
\contentsline {subsection}{\numberline {1.2}Motivaci\IeC {\'o}}{3}
\contentsline {subsection}{\numberline {1.3}Organitzaci\IeC {\'o} mem\IeC {\`o}ria}{3}
\contentsline {section}{\numberline {2}Antecedents}{3}
\contentsline {subsection}{\numberline {2.1}\textit {Rust}}{3}
\contentsline {subsection}{\numberline {2.2}Compiladors}{3}
\contentsline {subsection}{\numberline {2.3}Transcompiladors}{4}
\contentsline {subsubsection}{\numberline {2.3.1}Treballs previs}{4}
\contentsline {subsubsection}{\numberline {2.3.2}Eines utilitzades}{4}
\contentsline {section}{\numberline {3}Disseny i arquitectura}{5}
\contentsline {subsection}{\numberline {3.1}Arquitectura}{5}
\contentsline {subsection}{\numberline {3.2}Detalls}{6}
\contentsline {section}{\numberline {4}Proves}{6}
\contentsline {subsection}{\numberline {4.1}Que funciona?}{6}
\contentsline {subsection}{\numberline {4.2}Limitacions}{6}
\contentsline {section}{\numberline {5}Conclusions}{6}
\contentsline {subsection}{\numberline {5.1}L\IeC {\'\i }nies futures}{6}
