"""
RustVisitor class

Lluis Alonso Jane
Universitat de Barcelona
"""

import re
import json
from pycparser import c_ast

INDENTATION = 4

class RustVisitor(c_ast.NodeVisitor):
    """Visitor class that traverses the nodes of the AST and converts them
       into Rust code"""

    def __init__(self, application_path):
        self.values, self.arrays, self.global_vars, self.pointers, \
                     self.current_args = ([] for i in range(5))
        self.indentation = 0
        self.structs, self.enums = {}, {}
        self.currentfunction, self.current_iterator = "", ""
        #the equivalency data is loaded from the json file
        with open(application_path + "equivalencies.json") as json_file:
            json_data = json.load(json_file)
        self.special_types = json_data["equivalent_types"]
        self.equivalent_types = json_data["functions_with_equivalents"]
        self.functions_to_parse = json_data["functions_with_parsing"]
        self.pragmas = json_data["pragmas_equivalents"]

    def visit(self, node):
        method = 'visit_' + (node.__class__.__name__).lower()
        return getattr(self, method, self.generic_visit)(node)

    def generic_visit(self, node):
        if node is None:
            return ''
        return ''.join(self.visit(c) for c_name, c in node.children())

    ##############################################
    #           Main Visit Functions             #
    ##############################################

    def visit_fileast(self, node):
        """FileAST visitor"""
        #This is the initial node
        str_to_ret = ''
        for ext in node.ext:
            if isinstance(ext, c_ast.FuncDef):
                str_to_ret += self.visit(ext)
            elif isinstance(ext, c_ast.Pragma):
                str_to_ret += self.visit(ext)
            elif isinstance(ext, c_ast.Decl):
                if isinstance(ext.type, c_ast.Struct):
                    str_to_ret += self.visit(ext.type)
                elif isinstance(ext.type, c_ast.Enum):
                    str_to_ret += self._get_indent(self.indentation)
                    enum_values = self.visit(ext.type.values)
                    str_to_ret += "enum " + ext.type.name + " { " + enum_values +\
                                " }" + "\n"
                    self.enums[ext.type.name] = str.split(enum_values, ", ")
                else:
                    init_value = self.visit(ext.init)
                    var_name = re.sub(r":.*", "", self.visit(ext.type))
                    #We must keep a list of global variables as they need to be checked when used
                    self.global_vars.append(var_name)
                    if init_value != '':
                        if ";" in init_value:
                            str_to_ret += 'static mut ' + self.visit(ext.type) + " = " + \
                                          init_value.lstrip()
                        else:
                            str_to_ret += 'static mut ' + self.visit(ext.type) + " = " + \
                                          init_value + ";\n"
                    else:
                        #in Rust it's possible to declare the type of a variable implicitly on the
                        #first assignation, but we declare it anyway to prevent cases where the
                        #typing and the actual type do not match by Rust standards (ie, declaring a
                        #i32 as f32)
                        str_to_ret += 'static mut ' + self.visit(ext.type) + ";\n"
            else:
                str_to_ret += self.visit(ext) + ';\n'
        return str_to_ret

    def visit_assignment(self, node):
        """Assignement visitor"""
        str_to_ret = self._get_indent(self.indentation)
        assignement_value = self.visit(node.rvalue).lstrip()
        assignement_id = self.visit(node.lvalue)
        if isinstance(node.rvalue, c_ast.FuncCall):
            assignement_value = assignement_value.replace(";\n", "")
        elif isinstance(node.rvalue, c_ast.UnaryOp):
            u_op = self._get_equivalent_op(node.rvalue.op)
            assignement_value = u_op + self.visit(node.rvalue.expr)
        if assignement_id in self.global_vars or assignement_value in self.global_vars:
            str_to_ret += "unsafe {" + assignement_id + " " + node.op + " " + \
                          assignement_value + "};\n"
        else:
            str_to_ret += assignement_id + " " + node.op + " " + assignement_value + ";\n"
        return str_to_ret

    def visit_arraydecl(self, node):
        """ArrayDecl visitor"""
        #This first case happens when a function recieves arrays as a parameter
        if isinstance(node.type, c_ast.ArrayDecl):
            return self.visit(node.type)
        #The first time we declare an array we save the name
        name = self._get_array_name(node.type)
        if name not in self.arrays and name != "":
            self.arrays.append(name)
        str_to_ret = ''
        dim = self.visit(node.dim)
        if dim:
            str_to_ret += self.visit(node.type).replace(": ", ": [") + "; "
            str_to_ret += self.visit(node.dim)+"]"
        else:
            str_to_ret += self.visit(node.type).replace(": ", ": &[") + "]"
        return str_to_ret

    def visit_arrayref(self, node):
        """ArrayRef visitor"""
        #Note: Array indices must be of type usize in Rust, so it needs to be casted
        #Also note that usize is dependant on the system CPU type (32 or 64 bits)
        #The parenthesis are there so that the cast is properly done
        str_to_ret = self.visit(node.name) + "[(" + self.visit(node.subscript) + ") as usize]"
        return str_to_ret

    def visit_binaryop(self, node):
        """BinaryOp visitor"""
        left = self.visit(node.left).lstrip()
        right = self.visit(node.right).lstrip()
        if isinstance(node.left, c_ast.FuncCall):
            left = left.replace(";\n", "")
        if isinstance(node.right, c_ast.FuncCall):
            right = right.replace(";\n", "")

        #It is necessary to check if there are other operators and act accordingly,
        #so the evaluation of the declaration is correct
        #In this case, extreme correctness (all the parenthesis necessary) and a correct
        # result is better than an incorrect result because of lack of parenthesis
        if isinstance(node.left, c_ast.BinaryOp):
            left = "(" + left.replace(";\n", "") + ")"
        if isinstance(node.right, c_ast.BinaryOp):
            right = "(" + right.replace(";\n", "") + ")"
        #Any operation with global variables is unsafe in Rust, there's no way around it
        if left in self.global_vars or right in self.global_vars:
            str_to_ret = "unsafe{" +left.lstrip() + " " + node.op + " " + right.lstrip() + "}"
        else:
            str_to_ret = left.lstrip() + " " + node.op + " " + right.lstrip()
        return str_to_ret

    def visit_break(self, _):
        """Break visitor"""
        return self._get_indent(self.indentation) + "break;\n"

    def visit_case(self, node):
        """Case visitor"""
        str_to_ret = self._get_indent(self.indentation)
        str_to_ret += self.visit(node.expr) + " => {\n"
        self.indentation += INDENTATION
        str_to_ret += ''.join(self.visit(child) for child in node.stmts)
        self.indentation -= INDENTATION
        str_to_ret += self._get_indent(self.indentation) + "}\n"
        #Breaks must be replaced as the match statement does not need to break out of
        #each case
        str_to_replace = self._get_indent(self.indentation+INDENTATION) + "break;\n"
        str_to_ret = str_to_ret.replace(str_to_replace, "")
        return str_to_ret

    def visit_cast(self, node):
        """Cast visitor"""
        str_to_ret = "("
        str_to_ret += self.visit(node.expr).replace(");\n", ")").lstrip() + " as "
        str_to_ret += self.visit(node.to_type).replace(": ", "")
        str_to_ret += ")"
        return str_to_ret

    def visit_compound(self, node):
        """Compound visitor"""
        str_to_ret = self._get_indent(self.indentation) + '{\n'
        self.indentation += INDENTATION
        str_to_ret += ''.join(self.visit(c) for c in node.block_items)
        self.indentation -= INDENTATION
        str_to_ret += self._get_indent(self.indentation) + '}\n'
        return str_to_ret

    def visit_constant(self, node):
        """Constant visitor"""
        str_to_ret = self._check_string(node.value)
        return str_to_ret

    def visit_continue(self, _):
        """Continue visitor"""
        #because the for loop is a while loop in this case, we have to prepare the get to the next
        #iteration before a continue statement. It is presupposed that every break will be in an
        #if statement
        return self._get_indent(self.indentation) + self.current_iterator.lstrip() + \
               self._get_indent(self.indentation) + "continue;\n"

    def visit_decl(self, node):
        """Decl visitor"""
        #function declaration
        if isinstance(node.type, c_ast.FuncDecl):
            str_to_ret = self.visit(node.type)

        #struct declaration
        elif isinstance(node.type, c_ast.Struct):
            str_to_ret = self.visit(node.type)

        #struct initialization
        elif (isinstance(node.type, c_ast.TypeDecl) and
              isinstance(node.init, c_ast.InitList)):
            str_to_ret = self._get_indent(self.indentation)
            str_to_ret += 'let mut ' + self.visit(node.type)

            #extract the struct name
            struct_name = re.sub(r"let mut [^\:]+", "", str_to_ret)
            struct_name = struct_name.replace(" ", "").replace(":", "")
            if struct_name not in self.structs:
                raise KeyError("Struct " + struct_name + " referenced before declaration")
            init_value = self.visit_initlist(node.init, struct_name=struct_name,
                                             struct_values=self.structs[struct_name])
            str_to_ret += " = " + init_value + ";\n"
            return str_to_ret

        #array declaration with initialization
        elif (isinstance(node.type, c_ast.ArrayDecl) and
              isinstance(node.init, c_ast.InitList)):
            str_to_ret = self._get_indent(self.indentation)
            str_to_ret += 'let mut ' + self.visit(node.type)
            str_to_ret = re.sub(r":.*", "", str_to_ret)
            #it is not necessary to know the type of the array, as the case where a different
            #variable type is assigned implicitly cannot occur here under usual circumstances
            #it also makes it easier as we do not have to control the type (it's not trivial here)
            array_contents = self._visit_array_init(node.init)
            str_to_ret += " = ["
            str_to_ret += ", ".join(element for element in array_contents)
            str_to_ret += "];\n"
            return str_to_ret

        #array declaration without initialization
        elif isinstance(node.type, c_ast.ArrayDecl):
            str_to_ret = self._get_indent(self.indentation)
            str_to_ret += 'let mut ' + self.visit(node.type) + ";\n"
            return str_to_ret

        #pointer declaration
        elif isinstance(node.type, c_ast.PtrDecl):
            str_to_ret = self._get_indent(self.indentation)
            init_value = self.visit(node.init)
            node_decl = self._visit_pointer_decl(node.type.type)
            if ";" not in init_value:
                init_value += ";\n"
            str_to_ret += 'let mut ' + node_decl + " = " + init_value.lstrip()
            #We need to extract the name and add it to the pointer list
            name = re.sub(r":.*", "", str_to_ret).replace("\n", "").lstrip()
            name = re.sub(r"let mut ", "", name)
            self.pointers.append(name)
            return str_to_ret

        #enum declaration
        elif (isinstance(node.type, c_ast.TypeDecl) and
              isinstance(node.type.type, c_ast.Enum)):
            init_value = self.visit(node.init)
            str_to_ret = self._get_indent(self.indentation)
            if init_value != '':
                if ";" not in init_value:
                    init_value += ";\n"
                str_to_ret += 'let mut ' + self.visit(node.type) + " = " + init_value.lstrip()
            else:
                str_to_ret += 'let mut ' + self.visit(node.type) + ";\n"
            return str_to_ret

        #default case (variable declaration)
        else:
            init_value = self.visit(node.init)
            str_to_ret = self._get_indent(self.indentation)
            if init_value != '':
                if ";" not in init_value:
                    init_value += ";\n"
                str_to_ret += 'let mut ' + self.visit(node.type) + " = " + init_value.lstrip()
            else:
                #in Rust it's possible to declare the type of a variable implicitly on the first
                #assignation, but we declare it anyway to prevent cases where the typing and the
                #actual type do not match by Rust standards (ie, declaring a i32 as i64)
                str_to_ret += 'let mut ' + self.visit(node.type) + "" + ";\n"
        return str_to_ret

    def visit_default(self, node):
        """Default visitor"""
        str_to_ret = self._get_indent(self.indentation)
        str_to_ret += "_" + " => {\n"
        self.indentation += INDENTATION
        str_to_ret += ''.join(self.visit(child) for child in node.stmts)
        self.indentation -= INDENTATION
        str_to_ret += self._get_indent(self.indentation) + "}\n"
        #Breaks must be replaced as the match statement does not need to break out of
        #each case
        str_to_replace = self._get_indent(self.indentation+INDENTATION) + "break;\n"
        str_to_ret = str_to_ret.replace(str_to_replace, "")
        return str_to_ret

    def visit_dowhile(self, node):
        """DoWhile visitor"""
        str_to_ret = self._get_indent(self.indentation)
        str_to_ret += "loop " + self.visit(node.stmt).lstrip()
        end_cond = self._get_indent(self.indentation)+ "if !(" + self.visit(node.cond) \
                    + ") {break} \n"
        end_cond += self._get_indent(self.indentation) + "}\n"
        str_to_ret = str_to_ret[::-1].replace("\n}", "", 1)[::-1]
        str_to_ret += end_cond
        return str_to_ret

    def visit_enum(self, node):
        """Enum visitor"""
        return node.name

    def visit_enumerator(self, node):
        """Enumerator visitor"""
        return node.name

    def visit_enumeratorlist(self, node):
        """EnumeratorList visitor"""
        str_to_ret = ""
        str_to_ret += ", ".join(self.visit(enumerator) for enumerator in node.enumerators)
        return str_to_ret


    def visit_exprlist(self, node):
        """ExprList visitor"""
        str_to_ret = ''
        for expr in node.exprs:
            temp = self.visit(expr)
            #if the array is in the current argument list, we must not create
            #a new mutable reference to it (it would be a double reference and
            #cause a type missmatch)
            if temp in self.arrays and temp not in self.current_args:
                str_to_ret += "&mut " + temp + ", "
            elif temp in self.global_vars:
                str_to_ret += "unsafe{" + temp + "}" + ", "
            elif temp in self.pointers:
                str_to_ret += "&mut " + temp + ", "
            else:
                str_to_ret += temp.lstrip().rstrip() + ", "
        str_to_ret = str_to_ret[:-2]  #we remove the last comma and the space
        return str_to_ret

    def visit_expr(self, node):
        """Expr visitor"""
        if isinstance(node, c_ast.InitList):
            return '{' + self.visit(node) + '}'
        elif isinstance(node, c_ast.ExprList):
            return '(' + self.visit(node) + ')'
        return self.visit(node)

    def visit_for(self, node):
        """For visitor"""
        #we create a block so the int that goes out of scope when the for loop ends
        str_to_ret = self._get_indent(self.indentation) + "{\n"
        self.indentation += INDENTATION

        #We save the current iterator (node.next) in case of continues
        previous_iterator = self.current_iterator
        self.current_iterator = self.visit(node.next)

        str_to_ret += self.visit(node.init)
        str_to_ret += self._get_indent(self.indentation) + 'while '
        str_to_ret += self.visit(node.cond) + " "
        #we prepare the closing statement
        temp = self._get_indent(INDENTATION) + self.visit(node.next)
        temp += self._get_indent(self.indentation) + "}"

        #Default for statements
        if isinstance(node.stmt, c_ast.Compound):
            str_to_ret += self.visit(node.stmt).lstrip()
            s_to_replace = self._get_indent(self.indentation) + "}"
            str_to_ret = str_to_ret[::-1].replace(s_to_replace[::-1], "", 1)[::-1]
            #the for operation is added at the end of the block.
            str_to_ret += temp

        #One-line for statements
        else:
            self.indentation += INDENTATION
            str_to_ret += "{\n" + self._get_indent(self.indentation) + \
                          self.visit(node.stmt).lstrip()
            #the for operation is added at the end of the block.
            str_to_ret += temp
            self.indentation -= INDENTATION

        self.indentation -= INDENTATION
        #we close the block
        str_to_ret += "\n" + self._get_indent(self.indentation) + "}\n"

        self.current_iterator = previous_iterator

        return str_to_ret

    def visit_funccall(self, node):
        """FuncCall visitor"""
        str_to_ret = self._get_indent(self.indentation)
        name = self.visit(node.name)
        if name in self.functions_to_parse:
            method = "_analize_" + name
            return getattr(self, method)(node.args)
        #the arguments for the function call may be other function calls,
        #the ";" and the newline must be removed
        args = self.visit(node.args).replace(";", "").replace("\n", "").lstrip()
        str_to_ret += self.visit(node.name) + '(' + args + ');\n'
        return str_to_ret

    def visit_funcdecl(self, node):
        """FuncDecl visitor"""
        str_to_ret = self._get_indent(self.indentation)
        if isinstance(node.type, c_ast.TypeDecl):
            #we analise the function name and return type
            #if there's no type or it is void, it returns nothing
            #r[0] is the function's name, r[1] are the return types
            ret = self._get_return_type(node.type)
            if ret[1] != "":
                str_to_ret += ret[0] + '(' + self.visit(node.args)  + ')' + \
                              ' -> ' + self.special_types[ret[1]]
            else:
                str_to_ret += ret[0] + '(' + self.visit(node.args)  + ')'
        else:
            if isinstance(node.type, c_ast.PtrDecl):
                temp = self.visit(node.type)
                return_type = re.sub(r".*: ", "", temp)
                name = re.sub(r": .*", "", temp)
                str_to_ret += name + '(' + self.visit(node.args)  + ')' + \
                              " -> Box<[" + return_type + "]>"
            else:
                str_to_ret += self.visit(node.type) + '(' + self.visit(node.args)  + ')'
        return str_to_ret

    def visit_funcdef(self, node):
        """FuncDef visitor"""
        decl = self.visit(node.decl)
        body = self.visit(node.body)
        if node.param_decls:
            #this case would should never be given in normal reasons, as K&R
            #function declarations is deprecated in C99. It is included because
            #the library supports it and it's not a big deal
            knrdecls = ', '.join(self.visit(p).replace(";\n", "").replace("let ", "") \
                       for p in node.param_decls)
            final = "fn " + decl.replace(")", knrdecls + ")")  + ' ' + body + "\n"
            return final
        else:
            return 'fn ' + decl + ' ' + body + '\n'

    def visit_id(self, node):
        """ID visitor"""
        return self._check_id(node.name)

    def visit_initlist(self, node, struct_name=None, struct_values=None):
        """"InitList visitor"""
        str_to_ret = ""
        if struct_name and struct_values:
            str_to_ret += struct_name + " {"
            str_to_ret += ', '.join(value + " : " + self.visit(expr) \
                    for value, expr in zip(struct_values, node.exprs))
            str_to_ret += "}"
        return str_to_ret

    def visit_if(self, node):
        """If visitor"""
        str_to_ret = self._get_indent(self.indentation)
        cond = self.visit(node.cond)
        cond = cond.replace(";\n", "").lstrip()
        #if the conditional is of type "if (id)" the equivalent in rust must be
        #a full binary operator (in this case "if (id != 0)")
        if isinstance(node.cond, c_ast.ID):
            cond += " != 0 "
        str_to_ret += "if "+ cond + " "
        if isinstance(node.iftrue, c_ast.Compound):
            str_to_ret += self.visit(node.iftrue).lstrip()
        else:
            #there aren't one-line if statatements in Rust,
            #it translates into a traditional if statement
            self.indentation += INDENTATION
            str_to_ret += " {\n" + self.visit(node.iftrue)
            self.indentation -= INDENTATION
            str_to_ret += self._get_indent(self.indentation) + "}\n"
        if node.iffalse is not None:
            if not isinstance(node.iffalse, c_ast.If):
                str_to_ret += self._get_indent(self.indentation) + "else "
                if isinstance(node.iffalse, c_ast.Compound):
                    str_to_ret += self.visit(node.iffalse).lstrip()
                else:
                    #there aren't one-line if statatements in Rust
                    self.indentation += INDENTATION
                    str_to_ret += " {\n" + self.visit(node.iffalse)
                    self.indentation -= INDENTATION
                    str_to_ret += self._get_indent(self.indentation) + "}\n"
            else:
                str_to_ret += self._get_indent(self.indentation) + "else "
                if isinstance(node.iffalse, c_ast.Compound):
                    str_to_ret += self.visit(node.iffalse).lstrip()
                else:
                    #there aren't one-line if statatements in Rust
                    str_to_ret += "" + self.visit(node.iffalse).lstrip()
        return str_to_ret

    def visit_identifiertype(self, node):
        """IdentifierType visitor"""
        if node.names[0] == 'void':
            return ''
        if len(node.names) > 1:
            return '' + self._get_equivalent_type(node.names)
        else:
            return '' + node.names[0]

    def visit_paramlist(self, node):
        """ParamList visitor"""
        str_to_ret = ''
        params = []
        #children returns a tuple of 2, the node is always the 2nd (child),
        #the first one is the node's name (which we don't use)
        for _, child in node.children():
            #special case with K&R function declaration
            if isinstance(child, c_ast.ID):
                return ""
            if isinstance(child.type, c_ast.PtrDecl):
                params.append(self._visit_pointer_param(child.type))
                continue
            temp = self.visit(child)

            #we save the variable name to store it in the current function's
            #parameter list
            var_name = temp.replace("let mut", "").lstrip()
            var_name = re.sub(r":.*", "", var_name).replace("\n", "")
            self.current_args.append(var_name)
            if "let" in temp:
                #in C all the variables passed by parameter are mutable
                #so we keep the "mut" part
                temp = temp.replace("let ", '').replace(";\n", '')
            #arrays are mutable, but they must be passed by reference
            if "[" in temp:
                temp = temp.replace("mut ", "", )
                temp = temp.replace("[", "mut [")
                temp = re.sub(r";.*", "]", temp)
            params.append(temp)
        str_to_ret = ", ".join(params)
        return str_to_ret

    def visit_pragma(self, node):
        """Pragma visitor"""
        if node.string in self.pragmas:
            return self.pragmas[node.string] + "\n"
        elif "#pragma " + node.string in self.pragmas:
            return self.pragmas["#pragma " + node.string] + "\n"
        return ""

    def visit_return(self, node):
        """Return visitor"""
        str_to_ret = self._get_indent(self.indentation)
        str_to_ret += "return " + self.visit(node.expr).replace(";\n", "").lstrip() + ";\n"
        return str_to_ret

    def visit_struct(self, node):
        """Struct visitor"""
        str_to_ret = ""
        if node.decls is not None:
            str_to_ret += self._get_indent(self.indentation)
            str_to_ret += "struct " + node.name
            str_to_ret += self._get_indent(self.indentation) + " {\n"

            self.indentation += INDENTATION
            str_to_ret += ''.join(self.visit(c) for c in node.decls)
            self.indentation -= INDENTATION
            str_to_ret += self._get_indent(self.indentation) + "}\n\n"

            #variables inside a struct are only mutable if a struct is mutable
            #plus they don't have to be initialized
            str_to_ret = str_to_ret.replace("let mut ", "")
            str_to_ret = str_to_ret.replace(";", ",")
            struct_values = re.findall(r"[\w]+[\:]+", str_to_ret)
            struct_values = [word.replace(":", "") for word in struct_values]
            self.structs[node.name] = struct_values
            return str_to_ret
        else:
            str_to_ret += node.name
            return str_to_ret

    def visit_structref(self, node):
        """StructRef visitor"""
        str_to_ret = ""
        str_to_ret += self.visit(node.name) + node.type + self.visit(node.field)
        return str_to_ret

    def visit_switch(self, node):
        """Switch visitor"""
        str_to_ret = ""
        str_to_ret += self._get_indent(self.indentation)
        str_to_ret += "match (" + self.visit(node.cond) + ") "
        str_to_ret += self.visit(node.stmt).lstrip()
        return str_to_ret

    def visit_ternaryop(self, node):
        """TernaryOp visitor"""
        str_to_ret = ""
        cond = self.visit(node.cond).lstrip()
        if not any(comp in cond for comp in [">", "<", "="]):
            str_to_ret += "if (" + cond + ") != 0 {" + self.visit(node.iftrue) + "} "
        else:
            str_to_ret += "if (" +  cond + ") {" + self.visit(node.iftrue) + "} "
        str_to_ret += "else {" + self.visit(node.iffalse) + "}"
        return str_to_ret

    def visit_typedecl(self, node):
        """TypeDecl visitor"""
        str_to_ret = ""
        if node.declname is not None:   #this is the case of void
            str_to_ret += '' + node.declname
        decltype = self.visit(node.type)
        #if there is no type and no variable name, there's nothing
        #(this can happen in function parameter declarations)
        if decltype == "" and node.declname is None:
            return ""
        elif decltype in self.special_types:
            str_to_ret += ": " + self.special_types[decltype]
        else:
            str_to_ret += ": " + decltype
        return str_to_ret

    def visit_typedef(self, node):
        """TypeDef visitor"""
        str_to_ret = ""
        str_to_ret += "type " + self.visit(node.type).replace(":", " =") + ""
        return str_to_ret

    def visit_unaryop(self, node):
        """UnaryOp visitor"""
        str_to_ret = self._get_indent(self.indentation)

        #most unary operators are different or they only have a
        #binary operator equivalent
        u_op = self._get_equivalent_op(node.op)
        expr = self.visit(node.expr).lstrip()
        #the negation, pointer and minus are the only ones that come
        #before the statement, the others go after it
        if not any(i_op in u_op for i_op in ["*", "!", "-"]):
            str_to_ret += expr + u_op + "\n"
        elif "-=" in u_op:
            str_to_ret += expr + u_op + "\n"
        else:
            str_to_ret += u_op + expr
        return str_to_ret

    def visit_while(self, node):
        """While visitor"""
        str_to_ret = self._get_indent(self.indentation) + 'while '
        str_to_ret += self.visit(node.cond) + " " + self.visit(node.stmt).lstrip()
        return str_to_ret


    ##############################################
    #           Auxiliary Functions              #
    ##############################################
    def _check_id(self, name):
        #if the id is for a type or function that has an equivalent, we return it's equivalent
        if name in self.equivalent_types:
            return self.equivalent_types[name]
        #otherwise it's a declared function and the normal name is returned, unless it belongs
        #to an enum, then we have to add the enum's name.
        else:
            enum_mod = [key for key in self.enums if str(name) in self.enums[key]]
            if enum_mod:
                return enum_mod[0] + "::" + str(name)
            return str(name)

    def _get_return_type(self, node):
        if node.declname is None:
            return ''
        decltype = self.visit(node.type)
        ret = []
        #we change the current function we are defining and we reset the args list
        self.currentfunction = node.declname
        self.current_args = []
        ret.append(node.declname)
        ret.append(decltype)
        return ret

    def _check_string(self, value):
        temp = ''
        i = 0
        #substitution of the expressions %d, %lf per {0}, {2}, etc, done for print expressions
        regex = re.compile(r"%+([ugsdlfc\.])+(?:[0-9]?[lef]?[ef]?)")
        while temp != value:
            temp = value
            value = re.sub(regex, '{}', value, 1)
            i += 1
        return temp

    def _get_equivalent_op(self, oper):
        #the operators that are known to change do so
        if "++" in oper:
            return " += 1;"
        elif "--" in oper:
            return " -= 1;"
        elif "~" in oper:
            return " !"
        elif "&" in oper:
            return ""
        #otherwise it's assumed it's the same operator
        else:
            return oper
        #this exception would be the one to raise in case that an
        #exhaustive list of operators was made and it wasn't there
        #raise SyntaxError("Unknown Unary operator")

    def _get_indent(self, indent):
        return " " * indent

    def _visit_array_init(self, init):
        elements = [self.visit(element) for element in init.exprs]
        return elements

    def _get_equivalent_type(self, names):
        name = ' '.join(names)
        if name in self.special_types:
            return self.special_types[name]
        return name

    def _get_array_name(self, node):
        if hasattr(node, "declname"):
            return node.declname
        if node.type is not None:
            return self._get_array_name(node.type)
        return ""

    def _analize_malloc(self, args):
        str_to_ret = "Box::new("
        if isinstance(args.exprs[0], c_ast.BinaryOp):
            size = self._get_size_binaryop(args.exprs[0])
            str_to_ret += size + ");\n"
        elif not isinstance(args.exprs[0], c_ast.Constant):
            raise Exception("Error when initializing mallocs.")
        else:
            #if a number is recieved, we just reserve that number of bytes
            str_to_ret += "[0u8" + self.visit(args.exprs[0]) + "]);\n"
        return str_to_ret

    def _analize_calloc(self, args):
        str_to_ret = "Box::new("
        size = self.visit(args.exprs[0])
        if not isinstance(args.exprs[1], c_ast.UnaryOp):
            raise Exception("Dimension operator not supported for callocs.")
        var_type = self._get_size_unaryop(args.exprs[1])
        str_to_ret += "[0" + var_type + "; " + size + "]);\n"
        return str_to_ret

    def _analize_abs(self, args):
        str_to_ret = "("+self.visit(args) + ").abs();\n"
        return str_to_ret

    def _analize_free(self, _):
        return ""

    def _get_size_binaryop(self, binop):
        left = self.visit(binop.left)
        right = self.visit(binop.right)
        if isinstance(binop.left, c_ast.UnaryOp):
            left = self._get_size_unaryop(binop.left)
        if isinstance(binop.right, c_ast.UnaryOp):
            right = self._get_size_unaryop(binop.right)
        if "*" in binop.op:
            if "i" in right or "u" in right:
                left, right = right, left
        else:
            raise NotImplementedError("Operant not supported for mallocs.")
        return "[0"+left+"; "+right+"]"


    def _get_size_unaryop(self, unop):
        #this should always be a sizeof
        return self.visit(unop.expr).replace(": ", "")

    def _visit_pointer_param(self, pointer):
        str_to_ret = ""
        temp = self.visit(pointer.type)
        name = re.sub(r":.*", "", temp)
        vect = re.sub(r".*: ", "", temp)
        str_to_ret += name + ": &mut Box<["+vect+"]>"
        return str_to_ret

    def _visit_pointer_decl(self, node):
        str_to_ret = ""
        str_to_ret += '' + node.declname
        decltype = self.visit(node.type)
        #if there is no type and no variable name, there's nothing
        #(this can happen in function parameter declarations)
        if decltype == "":
            return ""
        if decltype in self.special_types:
            str_to_ret += ": Box<[" + self.special_types[decltype] + "]>"
        else:
            str_to_ret += ": Box<[" + decltype + "]>"
        return str_to_ret
