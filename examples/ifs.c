#include <stdio.h>


void number(int number)
{
    if (number == 1) printf("u\n");
    else if (number == 2) printf("dos\n");
    else if (number == 3) printf("tres\n");
    else if (number == 4) printf("quatre\n");
    else printf("desconegut\n");
}

void main(void)
{
    number(5);
    number(3);
}