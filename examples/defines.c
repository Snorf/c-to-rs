#include <stdio.h>
#define BONUSRATE1 0.1
#define BONUSRATE2 0.15
#define BONUSRATE3 0.2
#define SALES1 2000
#define SALES2 5000
#define SALES3 10000 
void main()
{
    float sales;
    float commission;

    sales = 15000.0;
    printf("Initial sales: %d.\n", sales);
    if(sales <= 2000.0)
    {
        commission = sales * BONUSRATE1;
        printf("Final comission: %g\n" , commission);
    }
    else if(sales > 2000.0 && sales <= 5000.0)
    {
        commission = sales * BONUSRATE2;
        printf("Final comission: %g\n" , commission);
    }
    else
    {
        commission = sales * BONUSRATE3;
        printf("Final comission: %g\n" , commission);
    }

}