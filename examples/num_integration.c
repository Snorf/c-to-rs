#include <stdio.h>

double func(double x)
{
    return x*3.0 + 3.0;
}

double int_leftrect(double from, double to, int n)
{
   double h = (to-from)/ (double)n;
   double sum = 0.0, x;
   for(x=from; x <= (to-h); x += h)
      sum += func(x);
   return h*sum;
}
 
double int_rightrect(double from, double to, int n)
{
   double h = (to-from)/ (double)n;
   double sum = 0.0, x;
   for(x=from; x <= (to-h); x += h)
     sum += func(x+h);
   return h*sum;
}
 
double int_midrect(double from, double to, int n)
{
   double h = (to-from)/ (double)n;
   double sum = 0.0, x;
   for(x=from; x <= (to-h); x += h)
     sum += func(x+h/2.0);
   return h*sum;
}
 
double int_trapezium(double from, double to, int n)
{
   double h = (to - from) / (double)n;
   double sum = func(from) + func(to);
   int i;
   for(i = 1;i < n;i++)
       sum += 2.0*func(from + (double)i * h);
   return  h * sum / 2.0;
}
 
double int_simpson(double from, double to, int n)
{
   double h = (to - from) / (double)n;
   double sum1 = 0.0;
   double sum2 = 0.0;
   int i;
 
   double x;
 
   for(i = 0;i < n;i++)
      sum1 += func(from + h * (double)i + h / 2.0);
 
   for(i = 1;i < n;i++)
      sum2 += func(from + h * (double)i);
 
   return h / 6.0 * (func(from) + func(to) + 4.0 * sum1 + 2.0 * sum2);
}

void main()
{
    double ic;

    double ivals[] = { 
    0.0, 1.0,
    1.0, 100.0,
    0.0, 5000.0,
    0.0, 6000.0
    };
    int approx[] = { 100, 1000, 5000000, 6000000 };
    ic = int_simpson(ivals[2], ivals[3], approx[0]);
    printf("Integrals de simpson entre %lf i %lf: %lf\n", ivals[2], ivals[3], ic);
    ic = int_trapezium(ivals[2], ivals[3], approx[0]);
    printf("Integrals per trapezi entre %lf i %lf: %lf\n", ivals[2], ivals[3], ic);
    ic = int_leftrect(ivals[2], ivals[3], approx[0]);
    printf("Integrals per rectangle entre %lf i %lf: %lf\n", ivals[2], ivals[3], ic);
  

}