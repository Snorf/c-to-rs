//The following program calculates the Costa's arrays of the 
//dimention defined by dim.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

#define dim 10

void print_matrix(int *matrix)
{
    printf("(");
    for (int i = 0; i < dim; i ++)
    {
        printf(" %d ", matrix[i]);
    }
    printf(")\n");
}

int can_be_costas(int *matrix, int level)
{
    for (int i = 1; i <= level - 1; i ++)
    {
        int maskP, maskN, mp, mn; 
        maskP = 1; //none can be at distance 0 (would imply repetition)
        maskN = 1;
        for (int j = level - 1; j >= i; j --)
        {
            int temp = matrix[j] - matrix[j - i];
            if (temp == 0) return 0; //repeated number (double check)
            if (temp > 0)
            {
                mp = 1 << temp;
                if ((maskP & mp) != 0) return 0;
                maskP |= mp;
            }
            else if (temp < 0)
            {
                mn = 1 << abs(temp);
                if ((maskN & mn) != 0) return 0;
                maskN |= mn;
            }
        }
    }
    //print_matrix(matrix);
    return 1;
}

int is_costas(int *matrix)
{
    for (int i = 1; i <= dim - 1; i ++)
    {
        int maskP, maskN, mp, mn; 
        maskP = 1; //none can be at distance 0 (would imply repetition)
        maskN = 1;
        for (int j = dim - 1; j >= i; j --)
        {
            int temp = matrix[j] - matrix[j - i];
            if (temp == 0) return 0; //repeated number (double check)
            if (temp > 0)
            {
                mp = 1 << temp;
                if ((maskP & mp) != 0) return 0;
                maskP |= mp;
            }
            else if (temp < 0)
            {
                mn = 1 << abs(temp);
                if ((maskN & mn) != 0) return 0;
                maskN |= mn;
            }
        }
    }
    //print_matrix(matrix);
    return 1;
}

int count_costas_matrices(int level, int *matrix)
{
    
    if (level == dim) return is_costas(matrix);
    int count = 0;
    for (int i = 1; i <= dim; i ++)
    {
        matrix[level] = i;
        char valid = 1;
        //if the number has been repeated, we don't check it
        for (int j = 0; j < level; j ++)
        {
            if (matrix[j] == matrix[level])
            {
                valid = 0;
                break;
            }
            if (!can_be_costas(matrix, level) == 1)
            {
                valid = 0;
                break;
            }
        }
        if (valid)
            count += count_costas_matrices(level + 1, matrix);
    }
    return count;
}

void main (void)
{
    int count = 0;
    int level = 0;
    int *matrix = calloc(dim, sizeof(int));
    count = count_costas_matrices(level, matrix);
    printf("Dimension: %d, number of matrices: %d\n", dim, count);
}