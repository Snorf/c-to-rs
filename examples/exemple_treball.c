#include <stdio.h>

void print_n(int n)
{
    for (int i = 0; i < n; i++)
        printf("Iteracio %d\n", i);
}

int add_one(int n)
{
    n++;
    return n;
}

void main()
{
    int n = 10;
    print_n(n);
    printf("\nPausa\n\n");
    print_n(add_one(n));
}