#include <stdio.h>

void main(void)
{
    int i;
    int a = 1;
    for (i = 0; i < 15; i++)
    {
        if (i > 0) a *= i;
        if (i == 3) continue;
        if (i == 9) break;
        printf("Iteracio actual: %d\n", i);
    }
    printf("Index actual de la i: %d\n", i);
    printf("Resultat del producte: %d\n", a);
    int b = -a;
    printf("Resultat del canviat de signe: %d\n", b);
}