#include <stdio.h>

void main(void)
{
  int item[10] = {2, 6, 3, 4, 7, 9, 1, 8, 5, 0}; 
  int x, y, t;
  int count = 10;

  for(t=0; t < count; t++){
    printf("%d ", item[t]);
  }
  printf(" -> End of unsorted list \n");

  for(x = 1; x < count; ++x)
    for(y = count-1; y >= x; --y) {
      if(item[ y - 1] > item[ y ]) {
        t = item[ y - 1];
        item[ y - 1] = item[ y ];
        item[ y ] = t;
      }
    }

  for(t=0; t < count; t++){
    printf("%d ", item[t]);
  }
  printf(" -> End of sorted list \n");

}