#include <stdio.h>

int main() {
   int j1,j2,j3;
   
   j1 = 2;
   j2 = 25;
  
   j3 = j1 & j2; 
   printf("j1 & j2 = %d\n",j3);
  
   j3 = j1 | j2; 
   printf("j1 | j2 = %d\n",j3);
  
   j3 = j1 ^ j2; 
   printf("i.e. j1 ^ j2 = %d\n",j3);
  
   j3 = ~j1; 
   printf("Compliment  of  j1 = %d\n",j3);
  
   j3 =  j1<<2;
   printf("Left shift by 2 bits j1 << 2 = %d\n",j3);
  
   j3 =  j1>>2;
   printf("Right shift by 2 bits j1 >> 2 = %d\n",j3);

}