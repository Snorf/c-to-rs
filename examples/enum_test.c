#include <stdio.h>

enum WebEvent {PageLoad, PageUnload, Test};

void inspect(enum WebEvent event)
{
    switch(event)
    {
        case PageLoad:
            printf("page loaded\n");
            break;
        case PageUnload:
            printf("page unloaded\n");
            break;
        default:
            printf("not recognised\n");
            break;
    }
}

void main(void)
{
    enum WebEvent load = PageLoad;
    enum WebEvent unload = PageUnload;
    enum WebEvent test;

    test = Test;

    inspect(load);
    inspect(unload);
    inspect(test);
}