//The following program checks if some given matrixes and
//its dimensions, those are latin squares.
#include <stdio.h>

int number_of_bits(int i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

char is_latin(int length, int input[])
{
    int maskCol, maskRow;
    int totalMask = 0;
    for (int i = 0; i < length; i ++)
    {
        maskCol = 0;
        maskRow = 0;
        for (int j = 0; j < length; j ++)
        {
            int maskRTemp = 1 << input[i*length + j];
            if ((maskRTemp & maskRow)!= 0) return 0;
            maskRow |= maskRTemp;
            
            int maskCTemp = 1 << input[j*length + i];
            if ((maskCTemp & maskCol) != 0) return 0;
            maskCol |= maskCTemp;
        }
        totalMask |= maskCol | maskRow;
    }
    return (number_of_bits(totalMask) != length) ? 0: 1;
}

void print_matrix(int input[], int length)
{
    for (int i = 0; i < length; i ++)
    {
        printf("(");
        for (int j = 0; j < length; j ++)
        {
            printf(" %d ", input[i*length + j]);
        }
        printf(")\n");
    }
}


void swap_columns(int length, int input[], int col1, int col2)
{
    int temp;
    for (int i = 0; i < length; i ++)
    {
        temp = input[col1*i];
        input[col1*i] = input[col2*i];
        input[col2*i] = temp;
    }
}

void swap_rows(int length, int input[], int row1, int row2)
{
    int temp;
    for (int i = 0; i < length; i ++)
    {
        temp = input[row1*length + i];
        input[row1*length + i] = input[row2*length + i];
        input[row2*length + i] = temp;
    }
}


void reduce(int length, int input[])
{
    for (int i = 0; i < length; i++)
    {
        int minRow = i;
        int minRowValue = input[i*length];
        for (int j = i+1; j <length; j ++)
        {
            if (input[j*length] < minRowValue)
            {
                minRowValue = input[j*length];
                minRow = j;
            }
        }
        if (minRow != i)
            swap_rows(length, input, minRow, i);
    }

    for (int i = 0; i < length; i ++)
    {
        int minCol = i;
        int minColValue = input[i];
        for (int j = i+1; j <length; j ++)
        {
            if (input[j] < minColValue)
            {
                minColValue = input[j];
                minCol = j;
            }
        }
        if (minCol != i)
            swap_columns(length, input, minCol, i);
    }
    print_matrix(input, length);
}

void main(void)
{
    char check;

    int length = 5;
    int input1[25] = {1, 2, 3, 4, 5, 5, 1, 2, 3, 4, 4, 5, 1, 2, 3, 3, 4, 5, 1, 2, 2, 3, 4, 5, 1};
    check = is_latin(length, input1);
    printf("Length: %d, is latin? %d\n", length, check);
    if (check) reduce(length, input1);
    
    length = 2;
    int input2[4] = {1, 3, 3, 4};
    check = is_latin(length, input2);
    printf("Length: %d, is latin? %d\n", length, check);
    if (check) reduce(length, input2);
    
    length = 4;
    int input3[16] = {1, 2, 3, 4, 1, 3, 2, 4, 2, 3, 4, 1, 4, 3, 2, 1};
    check = is_latin(length, input3);
    printf("Length: %d, is latin? %d\n", length, check);
    if (check) reduce(length, input3);
    
}