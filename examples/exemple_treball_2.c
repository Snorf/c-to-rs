#include <stdio.h>
#include <stdlib.h>

struct vessel 
{
    float first_var;
    int second_var;
};

void main(void)
{
    struct vessel ves = { 2.0, 4 };
    int *arr = malloc(sizeof(int)*4);
    int index = 0;
    do
    {
        arr[index] = ves.second_var;
        ves.second_var--;
        index++;
        printf("Second struct var is: %d\n", ves.second_var);
        printf("Last array assignement was: %d \n", arr[index-1]);
    } while ((int)ves.first_var < ves.second_var);
    free(arr);
}