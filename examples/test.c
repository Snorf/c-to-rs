#include <stdio.h>

typedef unsigned long long ulong;

struct vessel 
{
    float t;
    double s;
    char op;
};

int test(void)
{
    printf("Test\n");
    return 3;
}

int sumatori(int x)
{
    int temp = 0;
    if (temp == 0)
    {
        temp += 2;
    }
    for (int i = 0; i <= x; i++)          //mirar si convertir els for's a while's o intentar-los passar per for's de rust
    {
        temp += i;
    }
    int i = 0;
    while (i <= x)
    {
        temp += i;
        i++;
    }
    x = temp;
    return temp;
}

void test2()
{
    int i = 0;
    do {
        i++;
    }while (i < 10);
}

enum Strategy {RANDOM, IMMEDIATE, SEARCH};

void main(void)
{
    printf("Hello World!\n");
    int a = 3;
    a = 5 + a;
    {
        int b = a;
        short c;
    }
    int b = test();
    int d = sumatori(6);
    int e = sumatori(a);
    sumatori(a);
    printf("El text es: %d, %d\n", d, e);
    printf("Sera un error: %d, %d\n", a, b);
    struct vessel ve = { 0.1, 2.3, 3 };
    ve.t = 32.2;
    printf("Valors del struct: %f, %lf, %c\n", ve.t, ve.s, ve.op);
    double z = (double) sumatori(sumatori(test()));
    printf("Resultat de les nested functions %d\n", z);
    ulong be = 32;
    printf("El ulong es: %d\n", be);
    enum Strategy my_strategy = IMMEDIATE;
    printf("Strategy : %d\n", my_strategy);
}