#include <stdio.h>

typedef int integer;

void main(void)
{
    integer i;
    integer a = 1;
    for (i = 0; i < 15; i++)
    {
        if (i <= 3) continue;
        else if (i >= 9) break;
        else {
            a *= i;
            a = !(a%2) ? a/2 : a;
        } 
        printf("Current iteration: %d\n", i);
    }
    printf("Final result: %d\n", a);
}