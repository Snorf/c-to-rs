#include <stdio.h>
#include <stdlib.h>

void convert(int thousands, int hundreds, int tens, int ones)
{
  char *num[] = {"", "One", "Two", "Three", "Four", "Five", "Six",
	       "Seven", "Eight", "Nine"};

  char *for_ten[] = {"", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
		   "Seventy", "Eighty", "Ninty"};

  char *af_ten[] = {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
		  "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen"};

  printf("\nThe year in words is:\n");

  printf("%s thousand", num[thousands]);
  if (hundreds != 0)
    printf(" %s hundred", num[hundreds]);

  if (tens > 1)
    printf(" %s %s \n", for_ten[tens], num[ones]);
  else if (tens != 0)
    printf(" %s\n", af_ten[ones]);
  else
    printf(" and %s \n", num[ones]);
}


void main()
{
  int year;
  int n1000, n100, n10, n1;

  printf("Enter the year (4 digits): \n");
  scanf("%d", &year);
  //year = 2305;

  if (year > 9999 || year < 1000)
  {
    printf("Error !! The year must contain 4 digits.\n");
    //exit(1);
    return;
  }

  n1000 = year/1000;
  n100 = ((year)%1000)/100;
  n10 = (year%100)/10;
  n1 = ((year%10)%10);

  convert(n1000, n100, n10, n1);
}
