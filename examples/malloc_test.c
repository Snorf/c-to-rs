#include <stdio.h>
#include <stdlib.h>

void printer(int *pointer, int posicio)
{
    printf("p[%d] = %d\n", posicio, pointer[posicio]);
}


int main(void)
{
    printf("Malloc test 2\n");
    int *p = malloc(sizeof(int) * 3);
    p[0] = 0;
    p[1] = 3;
    int c = 15;
    p[2] = c;
    //aixo no es pot printar en Rust
    //printf("*p es %d\n", *p);
    /*for (int i = 0; i < 3; i++)
    {
        printf("p[%d] = %d \n", i, p[i]);
    }*/
    printer(p, 2);

}