                    C-to-rs project

What is it:
-----------
-This project aims to provide an easy way to transpile a C99 program to
 the Rust language.


Requirements to run:
--------------------
-Either python 2.7 or 3.6
-Pip installed (>sudo apt-get install python-pip in debian based distros)
-Pycparser installed (>pip install pycparser)
-Rust language installed (>curl https://sh.rustup.rs -sSf | sh)

Installation:
-------------
-Dependencies can be installed manually or alternatively doc/preliminars.sh 
 can be run (will only work in debian based distros with apt as their package
 manager).

Use:
----
-To run a test in /examples/ run:
	 >python c-to-rs.py examples/[filename.c]
	 >gcc examples/[filename.c] && ./a.out
	 >cd out/ && cargo run
 and it will output both compiler's outputs and both programs outputs' (if 
 both compile)
 
-To run on another file, run:
	 >python c-to-rs.py [filename]
	 >cd out/ && cargo run
 or alternatively:
	 >python c-to-rs.py [filename]
	 >rustc main.rs && ./main
 Note: the last option will not work if there are library dependencies in 
 the Rust project.

-For more options, run: 
     >python c-to-rs.py -h 

Limitations:
-----------
-See doc/Limitations.txt
